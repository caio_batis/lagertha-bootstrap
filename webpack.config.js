var path = require('path');

var context = path.resolve(__dirname, "app");
var outputPath = path.resolve(__dirname, "build");

module.exports = {
  context: context,
  entry: {
    main: 'main.js'
  },
  output: {
    path: outputPath,
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ['app', 'lagertha', 'node_modules']
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /(node_modules)/, loader: "babel", query: { presets: ['es2015'] } }
    ]
  }
};
